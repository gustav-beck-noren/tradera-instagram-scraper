const fetch = require('node-fetch');

const INSTAGRAM_ID = 331081721;
const NUMBER_OF_POSTS = 6;

const url = `https://www.instagram.com/graphql/query/?query_hash=d496eb541e5c789274548bf473cc553e&variables={"id":"${INSTAGRAM_ID}","first":${NUMBER_OF_POSTS}}`;

const cache = {
  lastFetch: 0,
  posts: [],
};

async function getPosts() {
  // first see if we have a cache in 30 mins
  const timeSinceLastFetch = Date.now() - cache.lastFetch;
  if (timeSinceLastFetch <= 1800000) {
    return cache.posts;
  }
  const data = await fetch(url).then(res => res.json());
  const posts = slimUpPosts(data);
  cache.lastFetch = Date.now();
  cache.posts = posts;
  return posts;
}

function slimUpPosts(response) {
  return response.data.user.edge_owner_to_timeline_media.edges.map(edge => ({
    large: edge.node.thumbnail_src,
    thumbnail: edge.node.thumbnail_resources[2].src,
    url: `https://instagram.com/p/${edge.node.shortcode}`,
    caption: edge.node.edge_media_to_caption.edges[0].node.text,
    id: edge.node.id,
  }));
}

exports.scrapeTraderaInstagram = async function(req, res) {
  try {
    const posts = await getPosts();

    if(posts.length < 1) {
      throw new Error('No posts found');
    }

    res.set('Content-Type', 'application/json');
    res.status(200).send(JSON.stringify(posts));

  } catch (error) {
    console.error(error.message, new Error('Scrape instagram failed')); // Will be logged to stackdriver
    res.status(500).send({ error: error.message })
  }
};